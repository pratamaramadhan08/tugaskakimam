const taks_4 = (num) => {
  if (num < 0) return "Number cant be negative"; //tidak boleh negative

  //mengubah bilangan menjadi 6
  let result = [];
  let temp;
  while (num !== 0) {
    temp = num % 6;
    num = (num - temp) / 6;
    result = [temp, ...result];
  }
  return result.join("");
};

console.log("Base 6 : " + taks_4(50));
